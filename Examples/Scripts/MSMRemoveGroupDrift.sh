#!/bin/bash 

get_batch_options() {
    local arguments=("$@")

    unset command_line_specified_study_folder
    unset command_line_specified_subj
    unset command_line_specified_run_local

    local index=0
    local numArgs=${#arguments[@]}
    local argument

    while [ ${index} -lt ${numArgs} ]; do
        argument=${arguments[index]}

        case ${argument} in
            --StudyFolder=*)
                command_line_specified_study_folder=${argument#*=}
                index=$(( index + 1 ))
                ;;
            --Subjlist=*)
                command_line_specified_subj=${argument#*=}
                index=$(( index + 1 ))
                ;;
            --runlocal)
                command_line_specified_run_local="TRUE"
                index=$(( index + 1 ))
                ;;
	    *)
		echo ""
		echo "ERROR: Unrecognized Option: ${argument}"
		echo ""
		exit 1
		;;
        esac
    done
}

get_batch_options "$@"

StudyFolder="/isi01/biobankdata/biobank_rosalind_complete/UKB210" #Location of Subject folders (named by subjectID)
Subjlist="1002055@1002078@1002351@1002495@1002652@1002691@1002836@1003180@1003389@1003528@1003698@1003712@1003721@1003785@1004118@1004120@1004166@1004333@1004508@1004613@1004625@1004790@1005003@1005133@1005228@1005353@1005709@1005937@1005984@1006075@1006420@1006479@1006644@1006656@1006683@1006725@1006748@1006819@1006842@1006878@1007023@1007590@1007786@1007868@1007894@1007926@1007993@1008048@1008081@1008217@1008304@1008316@1008457@1009766@1010077@1010250@1010273@1010346@1010574@1010590@1010960@1011039@1011062@1011124@1011191@1011419@1011426@1011478@1011507@1011650@1011879@1012091@1012420@1012607@1012656@1012906@1013301@1013424@1013532@1013597@1013639@1014188@1014196@1014275@1014304@1014331@1014468@1014543@1015026@1015225@1015408@1015652@1015988@1016056@1016118@1016226@1016595@1016613@1016676@1016703@1016817@1016852@1016916@1016982@1017122@1017167@1017208@1017271@1017344@1017383@1017501@1017519@1017751@1017831@1017873@1017952@1017991@1018003@1018272@1018502@1018515@1018588@1018854@1018979@1019065@1019178@1019279@1019403@1019467@1019563@1019633@1019944@1020000@1020196@1020217@1020246@1020329@1020373@1020494@1020518@1020726@1020962@1021078@1021087@1021150@1021379@1021440@1021728@1021890@1021977@1022047@1022187@1022403@1022458@1022489@1022490@1022552@1022702@1022788@1022840@1023011@1023090@1023106@1023256@1023339@1023482@1023530@1023541@1023775@1023854@1024002@1024132@1024247@1024273@1024338@1024380@1024444@1024483@1025401@1025455@1025925@1025969@1026006@1026021@1026214@1026239@1026501@1026615@1026623@1027539@1030404@1030482@1030763@1030791@1031075@1031162@1031396@1031418@1031420@1031776@1031860@1032040@1032086@1032095@1032345@1032487@1032733@1032794@1032879@1033129" 
#@ delimited list of subject IDs
EnvironmentScript="/home/lw19/HCPpipelines/Examples/Scripts/SetUpHCPPipeline.sh" #Pipeline environment script

if [ -n "${command_line_specified_study_folder}" ]; then
    StudyFolder="${command_line_specified_study_folder}"
fi

if [ -n "${command_line_specified_subj}" ]; then
    Subjlist="${command_line_specified_subj}"
fi

# Requirements for this script
#  installed versions of: FSL, Connectome Workbench (wb_command)
#  environment: HCPPIPEDIR, FSLDIR, CARET7DIR 

# NOTE: this script will error on subjects that are missing some fMRI runs that are specified in the MR FIX arguments

#Set up pipeline environment variables and software
source ${EnvironmentScript}

# Log the originating call
echo "$@"

#if [ X$SGE_ROOT != X ] ; then
#    QUEUE="-q long.q"
    QUEUE="-q long.q"
#fi

PRINTCOM=""
#PRINTCOM="echo"

########################################## INPUTS ########################################## 

#Scripts called by this script do assume they run on the results of the HCP minimal preprocesing pipelines from Q2

######################################### DO WORK ##########################################

HighResMesh="164"
LowResMesh="32"
RegName="DeDriftMSMAllUKB"
CommonFolder="${HCPPIPEDIR}/global"
GroupAverageName="DeDriftMSMAllUKB"
InRegName="MSMAll_Strain_2_d40_WRN"
TargetRegName="MSMSulc"




for Subject in $Subjlist ; do
	echo "    ${Subject}"
	
	if [ -n "${command_line_specified_run_local}" ] ; then
	    echo "About to run ${HCPPIPEDIR}/MSMAll/MSMAllPipeline.sh"
	    queuing_command=""
	else
	    echo "About to use fsl_sub to queue or run ${HCPPIPEDIR}/MSMAll/MSMAllPipeline.sh"
	    queuing_command="${FSLDIR}/bin/fsl_sub ${QUEUE}"
	fi

	${queuing_command} ${HCPPIPEDIR}/DeDriftAndResample/MSMRemoveGroupDrift.sh \
        --path=${StudyFolder} \
        --subject-list=${Subject} \
        --high-res-mesh=${HighResMesh} \
        --low-res-mesh=${LowResMesh} \
        --registration-name=${RegName} \
	--common-folder=${CommonFolder} \
	--group-average-name=${GroupAverageName} \
	--input-registration-name=${InRegName} \
	--target-registration-name=${TargetRegName}

done


