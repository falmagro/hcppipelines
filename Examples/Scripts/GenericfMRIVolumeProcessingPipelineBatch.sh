#!/bin/bash 

get_batch_options() {
    local arguments=("$@")

    unset command_line_specified_study_folder
    unset command_line_specified_subj
    unset command_line_specified_run_local

    local index=0
    local numArgs=${#arguments[@]}
    local argument

    while [ ${index} -lt ${numArgs} ]; do
        argument=${arguments[index]}

        case ${argument} in
            --StudyFolder=*)
                command_line_specified_study_folder=${argument#*=}
                index=$(( index + 1 ))
                ;;
            --Subject=*)
                command_line_specified_subj=${argument#*=}
                index=$(( index + 1 ))
                ;;
            --runlocal)
                command_line_specified_run_local="TRUE"
                index=$(( index + 1 ))
                ;;
	    *)
		echo ""
		echo "ERROR: Unrecognized Option: ${argument}"
		echo ""
		exit 1
		;;
        esac
    done
}

get_batch_options "$@"

StudyFolder="/data2/HCP_500_50" #Location of Subject folders (named by subjectID)
Subjlist="100307 100408 101006 101309 101915 102008 102816 103414 103515 103818 104820 105014 105115 105216 106016 106319 106521 107321 107422 108121 108323 108525 108828 109123 109325 110411 111413 111716 113215 113922 114924 117122 117324 118528 118730 118932 119833 120111 120212 120515 121618 122620 123117 123420 123925 124220 124422 124826 125525 126325 126628 127630 127933 128127 129028 130013 130316 130922 131722 131924 132118 133019 133625 133827 133928 134324 135225 135528 136227 136833 137128 137633 138231 138534 139637 140117 140824 140925 141422 141826 142828 143325 144226 144428 144832 145834 146432 147030 147737 148032 148840 148941 149337 149741 150423 150726 151223 151526 151627 151728 152831 153025 153833 154431 154734 154835 154936 155635 156233 156637 157336 157437 158136 158540 159138 159340 159441 160830 161327 161630 162026 162228 162329 162733 163129 163331 163432 163836 164030 164939 165032 165840 166438 167036 167743 168139 169343 171431 171633 172130 172332 172534 173334 173435 173536 173940 175439 176542 177645 177746 178748 178849 178950 179346 180129 180836 180937 181131 182739 182840 185139 186141 187143 187547 188347 189450 190031 191033 191336 191437 191841 192439 192540 192843 194140 194645 194847 195041 195647 195849 196144 196750 197550 198451 198855 199150 199251 199453 199655 199958 200109 200614 201111 201818 203418 204016 204521 205119 205220 205826 208024 208226 208327 209834 209935 210011 210415 210617 211114 211215 211316 211720 211922 212217 212318 212419 214019 214221 214423 214726 217126 217429 221319 224022 231928 233326 239944 245333 246133 250427 250932 251833 255639 280739 284646 285345 285446 289555 290136 293748 298051 298455 303119 303624 304020 308331 310621 316633 329440 334635 339847 351938 352738 361941 366042 366446 371843 377451 380036 382242 385450 386250 390645 395958 397760 397861 414229 415837 433839 436239 441939 445543 448347 465852 473952 479762 480141 485757 497865 499566 500222 510326 519950 522434 530635 531536 540436 541943 545345 547046 549757 552544 559053 562446 565452 567052 567961 568963 573249 573451 579665 580044 581349 583858 585862 586460 592455 594156 598568 599469 599671 601127 613538 620434 622236 623844 627549 638049 645551 665254 673455 677968 679568 680957 683256 685058 687163 690152 695768 702133 704238 705341 713239 715041 715647 725751 729557 732243 734045 742549 748662 749361 751348 753251 756055 759869 765056 770352 771354 779370 782561 784565 786569 789373 792564 792766 802844 814649 816653 826353 826454 833249 837560 837964 845458 849971 856766 857263 859671 861456 871762 871964 872764 877168 877269 885975 887373 889579 896778 896879 898176 899885 901139 901442 904044 910241 912447 922854 930449 932554 937160 951457 957974 958976 959574 965367 978578 979984 983773 984472 991267 992774 994273"

EnvironmentScript="/home/lw19/HCPpipelines/Examples/Scripts/SetUpHCPPipeline.sh" #Pipeline environment script

if [ -n "${command_line_specified_study_folder}" ]; then
    StudyFolder="${command_line_specified_study_folder}"
fi

if [ -n "${command_line_specified_subj}" ]; then
    Subjlist="${command_line_specified_subj}"
fi

# Requirements for this script
#  installed versions of: FSL, FreeSurfer, Connectome Workbench (wb_command), gradunwarp (HCP version)
#  environment: HCPPIPEDIR, FSLDIR, FREESURFER_HOME, CARET7DIR, PATH for gradient_unwarp.py

#Set up pipeline environment variables and software
source ${EnvironmentScript}

# Log the originating call
echo "$@"

#if [ X$SGE_ROOT != X ] ; then
#    QUEUE="-q long.q"
    QUEUE="-q hcp_priority.q"
#fi

if [[ -n $HCPPIPEDEBUG ]]
then
    set -x
fi

PRINTCOM=""
#PRINTCOM="echo"
#QUEUE="-q veryshort.q"

########################################## INPUTS ########################################## 

# Scripts called by this script do NOT assume anything about the form of the input names or paths.
# This batch script assumes the HCP raw data naming convention.
#
# For example, if phase encoding directions are LR and RL, for tfMRI_EMOTION_LR and tfMRI_EMOTION_RL:
#
#	${StudyFolder}/${Subject}/unprocessed/3T/tfMRI_EMOTION_LR/${Subject}_3T_tfMRI_EMOTION_LR.nii.gz
#	${StudyFolder}/${Subject}/unprocessed/3T/tfMRI_EMOTION_LR/${Subject}_3T_tfMRI_EMOTION_LR_SBRef.nii.gz
#
#	${StudyFolder}/${Subject}/unprocessed/3T/tfMRI_EMOTION_RL/${Subject}_3T_tfMRI_EMOTION_RL.nii.gz
#	${StudyFolder}/${Subject}/unprocessed/3T/tfMRI_EMOTION_RL/${Subject}_3T_tfMRI_EMOTION_RL_SBRef.nii.gz
#
#	${StudyFolder}/${Subject}/unprocessed/3T/tfMRI_EMOTION_LR/${Subject}_3T_SpinEchoFieldMap_LR.nii.gz
#	${StudyFolder}/${Subject}/unprocessed/3T/tfMRI_EMOTION_LR/${Subject}_3T_SpinEchoFieldMap_RL.nii.gz
#
#	${StudyFolder}/${Subject}/unprocessed/3T/tfMRI_EMOTION_RL/${Subject}_3T_SpinEchoFieldMap_LR.nii.gz
#	${StudyFolder}/${Subject}/unprocessed/3T/tfMRI_EMOTION_RL/${Subject}_3T_SpinEchoFieldMap_RL.nii.gz
#
# If phase encoding directions are PA and AP:
#
#	${StudyFolder}/${Subject}/unprocessed/3T/tfMRI_EMOTION_PA/${Subject}_3T_tfMRI_EMOTION_PA.nii.gz
#	${StudyFolder}/${Subject}/unprocessed/3T/tfMRI_EMOTION_PA/${Subject}_3T_tfMRI_EMOTION_PA_SBRef.nii.gz
#
#	${StudyFolder}/${Subject}/unprocessed/3T/tfMRI_EMOTION_AP/${Subject}_3T_tfMRI_EMOTION_AP.nii.gz
#	${StudyFolder}/${Subject}/unprocessed/3T/tfMRI_EMOTION_AP/${Subject}_3T_tfMRI_EMOTION_AP_SBRef.nii.gz
#
#	${StudyFolder}/${Subject}/unprocessed/3T/tfMRI_EMOTION_PA/${Subject}_3T_SpinEchoFieldMap_PA.nii.gz
#	${StudyFolder}/${Subject}/unprocessed/3T/tfMRI_EMOTION_PA/${Subject}_3T_SpinEchoFieldMap_AP.nii.gz
#
#	${StudyFolder}/${Subject}/unprocessed/3T/tfMRI_EMOTION_AP/${Subject}_3T_SpinEchoFieldMap_PA.nii.gz
#	${StudyFolder}/${Subject}/unprocessed/3T/tfMRI_EMOTION_AP/${Subject}_3T_SpinEchoFieldMap_AP.nii.gz
#
#
# Change Scan Settings: EchoSpacing, FieldMap DeltaTE (if not using TOPUP),
# and $TaskList to match your acquisitions
#
# If using gradient distortion correction, use the coefficents from your scanner.
# The HCP gradient distortion coefficents are only available through Siemens.
# Gradient distortion in standard scanners like the Trio is much less than for the HCP 'Connectom' scanner.
#
# To get accurate EPI distortion correction with TOPUP, the phase encoding direction
# encoded as part of the ${TaskList} name must accurately reflect the PE direction of
# the EPI scan, and you must have used the correct images in the
# SpinEchoPhaseEncode{Negative,Positive} variables.  If the distortion is twice as
# bad as in the original images, either swap the
# SpinEchoPhaseEncode{Negative,Positive} definition or reverse the polarity in the
# logic for setting UnwarpDir.
# NOTE: The pipeline expects you to have used the same phase encoding axis and echo
# spacing in the fMRI data as in the spin echo field map acquisitions.

######################################### DO WORK ##########################################

SCRIPT_NAME=`basename ${0}`
echo $SCRIPT_NAME

TaskList=""
TaskList+=" rfMRI_REST1_RL"  #Include space as first character
TaskList+=" rfMRI_REST1_LR"
TaskList+=" rfMRI_REST2_RL"
TaskList+=" rfMRI_REST2_LR"

# Start or launch pipeline processing for each subject
for Subject in $Subjlist ; do
  echo "${SCRIPT_NAME}: Processing Subject: ${Subject}"

  i=1
  for fMRIName in $TaskList ; do
    echo "  ${SCRIPT_NAME}: Processing Scan: ${fMRIName}"
	  
	TaskName=`echo ${fMRIName} | sed 's/_[APLR]\+$//'`
	echo "  ${SCRIPT_NAME}: TaskName: ${TaskName}"

	len=${#fMRIName}
	echo "  ${SCRIPT_NAME}: len: $len"
	start=$(( len - 2 ))
		
	PhaseEncodingDir=${fMRIName:start:2}
	echo "  ${SCRIPT_NAME}: PhaseEncodingDir: ${PhaseEncodingDir}"
		
	case ${PhaseEncodingDir} in
	  "PA")
		UnwarpDir="y"
		;;
	  "AP")
		UnwarpDir="y-"
		;;
	  "RL")
		UnwarpDir="x"
		;;
	  "LR")
		UnwarpDir="x-"
		;;
	  *)
		echo "${SCRIPT_NAME}: Unrecognized Phase Encoding Direction: ${PhaseEncodingDir}"
		exit 1
	esac
	
	echo "  ${SCRIPT_NAME}: UnwarpDir: ${UnwarpDir}"
		
    fMRITimeSeries="${StudyFolder}/${Subject}/unprocessed/3T/${fMRIName}/${Subject}_3T_${fMRIName}.nii.gz"

	# A single band reference image (SBRef) is recommended if available
	# Set to NONE if you want to use the first volume of the timeseries for motion correction
    fMRISBRef="${StudyFolder}/${Subject}/unprocessed/3T/${fMRIName}/${Subject}_3T_${fMRIName}_SBRef.nii.gz"
	
	# "Effective" Echo Spacing of fMRI image (specified in *sec* for the fMRI processing)
	# EchoSpacing = 1/(BWPPPE * ReconMatrixPE)
	#   where BWPPPE is the "BandwidthPerPixelPhaseEncode" = DICOM field (0019,1028) for Siemens, and
	#   ReconMatrixPE = size of the reconstructed image in the PE dimension
	# In-plane acceleration, phase oversampling, phase resolution, phase field-of-view, and interpolation
	# all potentially need to be accounted for (which they are in Siemen's reported BWPPPE)
    EchoSpacing="0.00058" 

	# Susceptibility distortion correction method (required for accurate processing)
	# Values: TOPUP, SiemensFieldMap (same as FIELDMAP), GeneralElectricFieldMap
    DistortionCorrection="TOPUP"

	# Receive coil bias field correction method
	# Values: NONE, LEGACY, or SEBASED
	#   SEBASED calculates bias field from spin echo images (which requires TOPUP distortion correction)
	#   LEGACY uses the T1w bias field (method used for 3T HCP-YA data, but non-optimal; no longer recommended).
	BiasCorrection="SEBASED"

	# For the spin echo field map volume with a 'negative' phase encoding direction
	# (LR in HCP-YA data; AP in 7T HCP-YA and HCP-D/A data)
	# Set to NONE if using regular FIELDMAP
    SpinEchoPhaseEncodeNegative="${StudyFolder}/${Subject}/unprocessed/3T/${fMRIName}/${Subject}_3T_SpinEchoFieldMap_LR.nii.gz"

	# For the spin echo field map volume with a 'positive' phase encoding direction
	# (RL in HCP-YA data; PA in 7T HCP-YA and HCP-D/A data)
	# Set to NONE if using regular FIELDMAP
    SpinEchoPhaseEncodePositive="${StudyFolder}/${Subject}/unprocessed/3T/${fMRIName}/${Subject}_3T_SpinEchoFieldMap_RL.nii.gz"

	# Topup configuration file (if using TOPUP)
	# Set to NONE if using regular FIELDMAP
    TopUpConfig="${HCPPIPEDIR_Config}/b02b0.cnf"

	# Not using Siemens Gradient Echo Field Maps for susceptibility distortion correction
	# Set following to NONE if using TOPUP
	MagnitudeInputName="NONE" #Expects 4D Magnitude volume with two 3D volumes (differing echo times)
    PhaseInputName="NONE" #Expects a 3D Phase difference volume (Siemen's style)
    DeltaTE="NONE" #2.46ms for 3T, 1.02ms for 7T
	
    # Path to General Electric style B0 fieldmap with two volumes
    #   1. field map in degrees
    #   2. magnitude
    # Set to "NONE" if not using "GeneralElectricFieldMap" as the value for the DistortionCorrection variable
    #
    # Example Value: 
    #  GEB0InputName="${StudyFolder}/${Subject}/unprocessed/3T/${fMRIName}/${Subject}_3T_GradientEchoFieldMap.nii.gz" 
    GEB0InputName="NONE"

	# Target final resolution of fMRI data
	# 2mm is recommended for 3T HCP data, 1.6mm for 7T HCP data (i.e. should match acquisition resolution)
	# Use 2.0 or 1.0 to avoid standard FSL templates
    FinalFMRIResolution="2"

	# Gradient distortion correction
	# Set to NONE to skip gradient distortion correction
	# (These files are considered proprietary and therefore not provided as part of the HCP Pipelines -- contact Siemens to obtain)
    # GradientDistortionCoeffs="${HCPPIPEDIR_Config}/coeff_SC72C_Skyra.grad"
    GradientDistortionCoeffs="NONE"

    # Type of motion correction
	# Values: MCFLIRT (default), FLIRT
	# (3T HCP-YA processing used 'FLIRT', but 'MCFLIRT' now recommended)
    MCType="MCFLIRT"
		
    if [ -n "${command_line_specified_run_local}" ] ; then
        echo "About to run ${HCPPIPEDIR}/fMRIVolume/GenericfMRIVolumeProcessingPipeline.sh"
        queuing_command=""
    else
        echo "About to use fsl_sub to queue or run ${HCPPIPEDIR}/fMRIVolume/GenericfMRIVolumeProcessingPipeline.sh"
        queuing_command="${FSLDIR}/bin/fsl_sub ${QUEUE}"
    fi

    ${queuing_command} ${HCPPIPEDIR}/fMRIVolume/GenericfMRIVolumeProcessingPipeline.sh \
      --path=$StudyFolder \
      --subject=$Subject \
      --fmriname=$fMRIName \
      --fmritcs=$fMRITimeSeries \
      --fmriscout=$fMRISBRef \
      --SEPhaseNeg=$SpinEchoPhaseEncodeNegative \
      --SEPhasePos=$SpinEchoPhaseEncodePositive \
      --fmapmag=$MagnitudeInputName \
      --fmapphase=$PhaseInputName \
      --fmapgeneralelectric=$GEB0InputName \
      --echospacing=$EchoSpacing \
      --echodiff=$DeltaTE \
      --unwarpdir=$UnwarpDir \
      --fmrires=$FinalFMRIResolution \
      --dcmethod=$DistortionCorrection \
      --gdcoeffs=$GradientDistortionCoeffs \
      --topupconfig=$TopUpConfig \
      --printcom=$PRINTCOM \
      --biascorrection=$BiasCorrection \
      --mctype=${MCType}

  # The following lines are used for interactive debugging to set the positional parameters: $1 $2 $3 ...

  echo "set -- --path=$StudyFolder \
      --subject=$Subject \
      --fmriname=$fMRIName \
      --fmritcs=$fMRITimeSeries \
      --fmriscout=$fMRISBRef \
      --SEPhaseNeg=$SpinEchoPhaseEncodeNegative \
      --SEPhasePos=$SpinEchoPhaseEncodePositive \
      --fmapmag=$MagnitudeInputName \
      --fmapphase=$PhaseInputName \
      --fmapgeneralelectric=$GEB0InputName \
      --echospacing=$EchoSpacing \
      --echodiff=$DeltaTE \
      --unwarpdir=$UnwarpDir \
      --fmrires=$FinalFMRIResolution \
      --dcmethod=$DistortionCorrection \
      --gdcoeffs=$GradientDistortionCoeffs \
      --topupconfig=$TopUpConfig \
      --printcom=$PRINTCOM \
      --biascorrection=$BiasCorrection \
      --mctype=${MCType}"

  echo ". ${EnvironmentScript}"
	
    i=$(($i+1))
  done
done
