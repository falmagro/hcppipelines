#!/bin/bash 

get_batch_options() {
    local arguments=("$@")

    unset command_line_specified_study_folder
    unset command_line_specified_subj
    unset command_line_specified_run_local

    local index=0
    local numArgs=${#arguments[@]}
    local argument

    while [ ${index} -lt ${numArgs} ]; do
        argument=${arguments[index]}

        case ${argument} in
            --StudyFolder=*)
                command_line_specified_study_folder=${argument#*=}
                index=$(( index + 1 ))
                ;;
            --Subjlist=*)
                command_line_specified_subj=${argument#*=}
                index=$(( index + 1 ))
                ;;
            --runlocal)
                command_line_specified_run_local="TRUE"
                index=$(( index + 1 ))
                ;;
	    *)
		echo ""
		echo "ERROR: Unrecognized Option: ${argument}"
		echo ""
		exit 1
		;;
        esac
    done
}

get_batch_options "$@"

StudyFolder="/isi01/biobankdata/UKB/UKB_MMP_Full" #Location of Subject folders (named by subjectID)
Subjlist="1037457 1037494 1037760 1037939 1038030 1038088 1039973 1040014 1040095 1040277 1040311 1040345 1040442 1040535 1042386 1042498 1042523 1042813 1042869 1042966 1043050 1043082 1043509 1043961 1044245 1044330 1044388 1044396 1044607 1044683 1044748 1044776 1044801 1045054 1045102 1045505 1045674 1045701 1046001 1046055 1046087 1046248 1046495 1046557 1046646 1052603 1052742 1053519 1053555 1053630 1053641 1053829 1054165 1054225 1054691 1054756 1055120 1055143 1056256 1056404 1056687 1056918 1057027 1057095 1057180 1057191 1057572 1057665 1057976 1058046 1058388 1058479 1058547 1058565 1058713 1058732 1058887 1058893 1058933 1058970 1059020 1059378 1059452 1059475 1059569 1059639 1059716 1059823 1059896 1060111 1060136 1060227 1060448 1060469 1060476 1060705 1033131 1033198 1033366 1033506 1036657 1036796 1037081 1037102 1037196 1037291 1037316" #Space delimited list of subject IDs
EnvironmentScript="/home/lw19/HCPpipelines/Examples/Scripts/SetUpHCPPipeline.sh" #Pipeline environment script

if [ -n "${command_line_specified_study_folder}" ]; then
    StudyFolder="${command_line_specified_study_folder}"
fi

if [ -n "${command_line_specified_subj}" ]; then
    Subjlist="${command_line_specified_subj}"
fi

# Requirements for this script
#  installed versions of: FSL, Connectome Workbench (wb_command)
#  environment: HCPPIPEDIR, FSLDIR, CARET7DIR 

# NOTE: this script will error on subjects that are missing some fMRI runs that are specified in the MR FIX arguments

#Set up pipeline environment variables and software
source ${EnvironmentScript}

# Log the originating call
echo "$@"

#if [ X$SGE_ROOT != X ] ; then
#    QUEUE="-q long.q"
    QUEUE="-q long.q"
#fi

PRINTCOM=""
#PRINTCOM="echo"

########################################## INPUTS ########################################## 

#Scripts called by this script do assume they run on the results of the HCP minimal preprocesing pipelines from Q2

######################################### DO WORK ##########################################

HighResMesh="164"
LowResMesh="32"
RegName="MSMAll_Strain_2_d40_WRN"
DeDriftRegFiles="${HCPPIPEDIR}/global/MNINonLinear/DeDriftMSMAllUKB.L.sphere.DeDriftMSMAllUKB.164k_fs_LR.surf.gii@${HCPPIPEDIR}/global/MNINonLinear/DeDriftMSMAllUKB.R.sphere.DeDriftMSMAllUKB.164k_fs_LR.surf.gii"
ConcatRegName="MSMAll_DeDrift"
Maps="sulc curvature corrThickness thickness"
MyelinMaps="MyelinMap SmoothedMyelinMap" #No _BC, this will be reapplied
MRFixConcatNames="NONE"
MRFixNames="NONE"
fixNames="filtered_func_data_clean_MNI" #Space delimited list or NONE
#fixNames="NONE" #Space delimited list or NONE
dontFixNames="" #Space delimited list or NONE
#dontFixNames="NONE"
SmoothingFWHM="2" #Should equal previous grayordinates smoothing (because we are resampling from unsmoothed native mesh timeseries)
HighPass="-1"
MotionRegression="FALSE"
MatlabMode="2" #Mode=0 compiled Matlab, Mode=1 interpreted Matlab, Mode=2 octave
#MatlabMode="0" #Mode=0 compiled Matlab, Mode=1 interpreted Matlab, Mode=2 octave

Maps=`echo "$Maps" | sed s/" "/"@"/g`
MyelinMaps=`echo "$MyelinMaps" | sed s/" "/"@"/g`
MRFixNames=`echo "$MRFixNames" | sed s/" "/"@"/g`
fixNames=`echo "$fixNames" | sed s/" "/"@"/g`
dontFixNames=`echo "$dontFixNames" | sed s/" "/"@"/g`

for Subject in $Subjlist ; do
	echo "    ${Subject}"
	
	if [ -n "${command_line_specified_run_local}" ] ; then
	    echo "About to run ${HCPPIPEDIR}/MSMAll/MSMAllPipeline.sh"
	    queuing_command=""
	else
	    echo "About to use fsl_sub to queue or run ${HCPPIPEDIR}/MSMAll/MSMAllPipeline.sh"
	    queuing_command="${FSLDIR}/bin/fsl_sub ${QUEUE}"
	fi

	mv ${StudyFolder}/${Subject}/fMRI/rfMRI.ica/filtered_func_data_clean_MNI.L.native.func.gii ${StudyFolder}/${Subject}/MNINonLinear/Results/filtered_func_data_clean_MNI/filtered_func_data_clean_MNI.L.native.func.gii

	mv ${StudyFolder}/${Subject}/fMRI/rfMRI.ica/filtered_func_data_clean_MNI.R.native.func.gii ${StudyFolder}/${Subject}/MNINonLinear/Results/filtered_func_data_clean_MNI/filtered_func_data_clean_MNI.R.native.func.gii

	cp -r ${StudyFolder}/${Subject}/fMRI/rfMRI.ica/filtered_func_data.ica ${StudyFolder}/${Subject}/MNINonLinear/Results/filtered_func_data_clean_MNI/filtered_func_data_clean_MNI.ica/filtered_func_data.ica


	${queuing_command} ${HCPPIPEDIR}/DeDriftAndResample/DeDriftAndResamplePipelineUKB.sh \
        --path=${StudyFolder} \
        --subject=${Subject} \
        --high-res-mesh=${HighResMesh} \
        --low-res-meshes=${LowResMesh} \
        --registration-name=${RegName} \
        --dedrift-reg-files=${DeDriftRegFiles} \
        --concat-reg-name=${ConcatRegName} \
        --maps=${Maps} \
        --myelin-maps=${MyelinMaps} \
        --multirun-fix-concat-names=${MRFixConcatNames} \
        --multirun-fix-names=${MRFixNames} \
        --fix-names=${fixNames} \
        --dont-fix-names=${dontFixNames} \
        --smoothing-fwhm=${SmoothingFWHM} \
        --highpass=${HighPass} \
        --matlab-run-mode=${MatlabMode} \
        --motion-regression=${MotionRegression}
done


