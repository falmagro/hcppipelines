#!/bin/zsh

for Subject in $( cat </home/lw19/Desktop/UKB_remaining_MSMAll_First_Run.txt) ; do 

StudyFolder=/isi01/biobankdata/UKB/UKB_MMP_Full
SubjectFolder=${StudyFolder}/${Subject}

echo ${Subject}

FSLDIR=/usr/local/fsl
source "$FSLDIR/etc/fslconf/fsl.sh"

SmoothingFWHM=2

Sigma=`echo "$SmoothingFWHM / ( 2 * ( sqrt ( 2 * l ( 2 ) ) ) )" | bc -l`

Caret7_Command="${CARET7DIR}"/wb_command

AtlasFolder="${SubjectFolder}/MNINonLinear" #brainmask, wmparc, ribbon
fMRIName=filtered_func_data_clean_MNI


WD=${SubjectFolder}/MNINonLinear/Results/${fMRIName}
FieldMap=${SubjectFolder}/fieldmap

SpinEchoMean=${FieldMap}/fieldmap_iout_mean_ud.nii.gz

SpinEchoMean2Struct=${FieldMap}/fieldmap_iout_to_T1.mat


SBRefStandard=${SubjectFolder}/fMRI/rfMRI.ica/reg/example_func2standard.nii.gz

Struct2Standard=${SubjectFolder}/T1/transforms/T1_to_MNI_warp.nii.gz

SpinEchoMeanStandard=${FieldMap}/fieldmap_iout_mean_ud_to_standard.nii.gz

if [ ! -f "${SpinEchoMeanStandard}" ] ; then 

${FSLDIR}/bin/applywarp --in=${SpinEchoMean} --ref=${FSLDIR}/data/standard/MNI152_T1_2mm.nii.gz --out=${SpinEchoMeanStandard} --warp=${Struct2Standard} --premat=${SpinEchoMean2Struct}

else 

echo ${SpinEchoMeanStandard} exists

fi

#take inputs from specified directory (likely some working dir), so we don't have to put initial-registration files into the output folders temporarily

${FSLDIR}/bin/imcp ${SBRefStandard} ${WD}/GRE.nii.gz


${FSLDIR}/bin/fslmaths ${SpinEchoMeanStandard} -div ${WD}/GRE.nii.gz ${WD}/SEdivGRE.nii.gz
${FSLDIR}/bin/fslmaths ${WD}/SEdivGRE.nii.gz -mas ${AtlasFolder}/T1_brain_mask.nii.gz ${WD}/SEdivGRE_brain.nii.gz

#${FSLDIR}/bin/fslmaths ${WD}/SEdivGRE_brain.nii.gz -thr 1.25 -uthr 2.2 ${WD}/SEdivGRE_brain_thr.nii.gz #perhaps a bit too aggressive
Median=`wb_command -volume-stats ${WD}/SEdivGRE_brain.nii.gz -roi ${WD}/SEdivGRE_brain.nii.gz -reduce MEDIAN`
STDev=`fslstats ${WD}/SEdivGRE_brain.nii.gz -S`
Lower=`echo "${Median}-${STDev}/3" | bc -l`
Upper=`echo "${Median}+${STDev}/3" | bc -l`
echo "Median=${Median}, STDev=${STDev}, Lower=${Lower}, Upper=${Upper}"
fslmaths ${WD}/SEdivGRE_brain.nii.gz -thr ${Lower} -uthr ${Upper} ${WD}/SEdivGRE_brain_thr.nii.gz

fslmaths ${WD}/SEdivGRE_brain_thr.nii.gz -ero -dilM -dilM -dilM -dilM -dilM -dilM -dilM -dilM -dilM -dilM -dilM -dilM -dilM -dilM ${WD}/${fMRIName}_pseudo_transmit_raw.nii.gz

${FSLDIR}/bin/fslmaths ${WD}/SEdivGRE_brain_thr.nii.gz -bin ${WD}/SEdivGRE_brain_thr_roi.nii.gz
${FSLDIR}/bin/fslmaths ${WD}/SEdivGRE_brain_thr.nii.gz -s 5 ${WD}/SEdivGRE_brain_thr_s5.nii.gz
${FSLDIR}/bin/fslmaths ${WD}/SEdivGRE_brain_thr_roi.nii.gz -s 5 ${WD}/SEdivGRE_brain_thr_roi_s5.nii.gz
${FSLDIR}/bin/fslmaths ${WD}/SEdivGRE_brain_thr_s5.nii.gz -div ${WD}/SEdivGRE_brain_thr_roi_s5.nii.gz -mas ${AtlasFolder}/T1_brain_mask.nii.gz ${WD}/SEdivGRE_brain_bias.nii.gz

${FSLDIR}/bin/fslmaths ${WD}/SEdivGRE_brain_bias.nii.gz -dilM -dilM ${WD}/${fMRIName}_pseudo_transmit_field.nii.gz

${FSLDIR}/bin/fslmaths ${SpinEchoMeanStandard} -mas ${AtlasFolder}/T1_brain_mask.nii.gz -div ${WD}/SEdivGRE_brain_bias.nii.gz ${WD}/SpinEchoMean_brain_BC.nii.gz

${FSLDIR}/bin/fslmaths ${WD}/GRE.nii.gz -mas ${AtlasFolder}/T1_brain_mask.nii.gz -div ${WD}/SpinEchoMean_brain_BC.nii.gz ${WD}/SE_BCdivGRE_brain.nii.gz

#${FSLDIR}/bin/fslmaths ${WD}/SE_BCdivGRE_brain.nii.gz -uthr 0.5 -bin ${WD}/Dropouts.nii.gz
${FSLDIR}/bin/fslmaths ${WD}/SE_BCdivGRE_brain.nii.gz -uthr 0.6 -bin ${WD}/Dropouts.nii.gz #Adjust to 60% SE signal to compensate for above change that slightly penalizes dropout finding
${FSLDIR}/bin/fslmaths ${WD}/Dropouts.nii.gz -dilD -s ${Sigma} ${WD}/${fMRIName}_dropouts.nii.gz

done

